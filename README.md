# Homelab App of Apps
A collection of Argo CD `Application` and `Project` definitons that describe sibling applications that Argo CD should sync and manage within the homelab Kubernetes cluster.